import sys
from PyQt5 import QtWidgets
# from PyQt5.QtWidgets import *
from PyQt5 import uic

from hash_encrypt import Hash_Encrypt

class MainUI():
    def __init__(self):
        self.pwd_hash_ui = uic.loadUi("hash_pwd.ui")
        self.pwd_hash_ui.show()

        # 加密
        self.hash = Hash_Encrypt()
        # 生成密钥按钮
        self.pwd_hash_ui.pwd_hash_button.clicked.connect(self.pwd_hash)

    # 生成密钥
    def pwd_hash(self):
        try:
            password = self.pwd_hash_ui.pwd_input.text()
            sign = self.pwd_hash_ui.sign_input.text()
            if not all([sign,password]):
                QtWidgets.QMessageBox.warning(self.pwd_hash_ui, "警告", "关联或密码不能为空！")
                return
            payloads = {
                "sign": sign,
                "password": password,
            }
            data = self.hash.encrypt(payloads)
            self.pwd_hash_ui.sign_input.setText(None)
            self.pwd_hash_ui.pwd_input.setText(None)
            self.pwd_hash_ui.pwd_hash_text.setText(data)
        except Exception as e:
            QtWidgets.QMessageBox.critical(self.pwd_hash_ui, "加密失败！",f"{e}")
# UI
from static import image

if __name__ == '__main__':
    app = QtWidgets.QApplication(sys.argv)
    ui = MainUI()
    app.exec()

# 打包命令
# pyinstaller -D -w -i hash_pwd.ico hash_pwd.py