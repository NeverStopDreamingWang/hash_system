import json,base64,hmac
import os
from pathlib import Path

from hash_encrypt import Hash_Encrypt
from aes_crypto import Aes_Encrypt

import datetime

# 密码系统
class Pwd_system():
    PWD_BASE_DIR = os.path.join(Path(__file__).resolve().parent,"pwd")
    def __init__(self):
        self.hash = Hash_Encrypt()
        self.aes = Aes_Encrypt()
        self.user = {}
        self.pwd_file = None
        if not os.path.exists(self.PWD_BASE_DIR):
            os.makedirs(self.PWD_BASE_DIR)

    # 注册用户
    def register(self, user, password):
        try:
            payloads = {
                user: password,
            }
            user_sign = self.hash.encrypt(payloads)

            self.write_file(self.pwd_file, "a+", f"————pwd_text————\n")
            self.write_file(self.pwd_file, "a+", f"{user_sign}\n")
        except Exception as e:
            return f"系统错误{e}！"
    # 登录
    def login(self,user,password):
        try:
            user_pwd = self.hash.encrypt(user)
            self.pwd_file = os.path.join(self.PWD_BASE_DIR, f"{user_pwd}.txt")
            if not os.path.isfile(self.pwd_file):
                self.register(user,password)

            user_hash = self.read_file(self.pwd_file)
            user_hash = user_hash[1].rstrip("\n")
            self.user["hash"] = user_hash
            payloads = {
                user: password,
            }
            pwd = self.hash.encrypt(payloads)
            if pwd == self.user.get("hash"):
                self.user["username"] = user
                self.user["password"] = password
                return True
            return f"密码错误！请重新输入！"
        except Exception as e:
            return f"系统错误{e}！"

    # 读取文件
    def read_file(self, file):
        try:
            file_content = []
            with open(file, "r", encoding="utf-8") as f:
                file_content = f.readlines()
                f.close()
            return file_content
        except Exception as e:
            return f"系统错误{e}！"

    # 写入文件
    def write_file(self, file, mode, data):
        try:
            with open(file, mode, encoding="utf-8") as f:
                f.write(data)
                f.close()
        except Exception as e:
            return f"系统错误{e}！"

    # 查看所有联系
    def sign_list(self):
        try:
            user_hash = self.read_file(self.pwd_file)
            user_hash = user_hash[2:]
            if len(user_hash) == 0:
                return [{"sign": "暂无数据","password": None}]

            password = self.user.get("password")
            temp = []
            for j,i in enumerate(user_hash):
                i = i.split("$")[1]
                data = self.aes.decrypt(password,j+1,i)
                temp.append(data)
            return temp
        except Exception as e:
            return f"系统错误{e}！"

    # 录入密码
    def push_sign(self,sign,password):
        try:
            payloads = {
                "sign": sign,
                "password": password,
                "datetime": str(datetime.datetime.now()),
            }
            hash_pwd = self.hash.encrypt(payloads)

            user_hash = self.read_file(self.pwd_file)
            user_hash = user_hash[2:]
            for i in user_hash:
                if hash_pwd == i.split("$")[0]:
                    return True
            user_password = self.user.get("password")
            pwd = self.aes.encrypt(user_password, len(user_hash) + 1, payloads)
            self.write_file(self.pwd_file,"a+",f"{hash_pwd}${pwd}\n")
            return True
        except Exception as e:
            return f"系统错误{e}！"
