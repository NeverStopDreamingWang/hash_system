import json,base64,hmac


class Hash_Encrypt():
    SECRET_KEY = b'b5x5&36jt+xykbcvrsii$leay_s!0+u_%bre@q)vzm+(ta%2sx'
    def __init__(self):
        self.alg = "SHA256"

    # 生成密钥
    def encrypt(self, payload):
        try:
            if payload == None:
                raise ValueError(f"payload 值不能为空！")
            payload = json.dumps(payload)
            grop = self.base64(payload).encode()
            sign = self.encrypt_sign(self.SECRET_KEY, grop, self.alg)
            return self.base64(sign)
        except Exception as e:
            raise ValueError(f"加密失败！{e}")

    # 封装 base64 编码
    def base64(self,d):
        try:
            s = d.encode()
            b = base64.urlsafe_b64encode(s)
            b = b.decode().strip("=")
            return b
        except Exception as e:
            raise ValueError(f"加密失败！{e}")

    # 加密
    def encrypt_sign(self,key, s_grop, auth):
        try:
            encrypt = hmac.new(key, s_grop, digestmod=auth)
            return encrypt.hexdigest()
        except Exception as e:
            raise ValueError(f"加密失败！{e}")


# if __name__ == '__main__':
#     sign = input("关联：")
#     password = input("密码：")
#     payloads = {
#         "sign": sign,
#         "password": password,
#     }
#     hash = Hash_pwd()
#     pwd = hash.encrypt_pwd(sign,password)
#     # time.sleep(5)
#     print(pwd)
