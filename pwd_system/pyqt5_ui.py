
from functools import partial
import sys
from PyQt5.QtCore import *
from PyQt5.QtGui import *
from PyQt5.QtWidgets import *
from PyQt5 import uic

# 后台系统
from pwd_hash_system import Pwd_system

class MainUI():
    def __init__(self):
        self.login_ui = uic.loadUi("UI/login.ui")
        self.index_ui = uic.loadUi("UI/index.ui")
        self.login_ui.show()

        # 挂载后台
        self.sys = Pwd_system()
        self.sign_list = []
        # 登录按钮
        self.login_ui.login_button.clicked.connect(self.login)

    # 登录
    def login(self):
        user = self.login_ui.user_input.text()
        password = self.login_ui.pwd_input.text()
        if not all([user,password]):
            QMessageBox.warning(self.login_ui, "警告", "账号或密码不能为空！")
            return
        data = self.sys.login(user,password)
        if data == True:
            self.login_ui.close()
            self.index()
        else:
            QMessageBox.critical(self.login_ui, "登陆失败", data)

    # 主页
    def index(self):
        try:
            self.index_ui.show()
            self.index_ui.push_pwd_button.clicked.connect(self.push_pwd)
            self.show_sign()
        except Exception as e:
            print("组件错误",e)

    # 添加密码
    def push_pwd(self):
        try:
            user = self.index_ui.sign_input.text()
            password = self.index_ui.pwd_input.text()
            if not all([user, password]):
                QMessageBox.warning(self.index_ui, "警告", "联系或密码不能为空！")
                return
            data = self.sys.push_sign(user, password)
            self.index_ui.sign_input.setText(None)
            self.index_ui.pwd_input.setText(None)
            if data == True:
                self.show_sign()
                QMessageBox.about(self.index_ui, "录入成功", "录入成功!")
            else:
                QMessageBox.critical(self.index_ui, "录入失败", data)
        except Exception as e:
            print("组件错误",e)

    # 展示列表
    def show_sign(self):
        try:
            self.index_ui.pwd_list.clear()
            # 获取数据
            self.sign_list = self.sys.sign_list()
            self.index_ui.pwd_list.itemClicked.connect(self.show_pwd)
            for i in self.sign_list:
                sign = i.get("sign")
                datetime = i.get("datetime")
                self.index_ui.pwd_list.addItem(f"{sign}\t\t{datetime}")
        except Exception as e:
            print("组件错误",e)

    # 展示联系密码
    def show_pwd(self,item):
        try:
            text = item.text()
            sign = text.split()[0]
            datetime = " ".join(text.split()[1:])
            for i in self.sign_list:
                if sign == i.get("sign") and datetime == i.get("datetime"):
                    password = i.get("password")
                    QMessageBox.about(self.index_ui, sign, f"关联：{sign}\n密码：{password}\n录入时间：{datetime}")
        except Exception as e:
            print("组件错误",e)