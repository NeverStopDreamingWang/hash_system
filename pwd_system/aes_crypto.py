from Crypto.Cipher import AES
import json,base64,binascii,re



class Aes_Encrypt():
    model = AES.MODE_CBC  # 定义模式
    coding = None

    # 补全16字符
    def secret_key16(self,secret_key):
        secret_key = str(secret_key)
        padding_size = 16 - len(secret_key) % 16
        padding_text = padding_size * "0"

        secret_key += padding_text
        return secret_key.encode('utf-8')

    # 补全 16字节
    def pkcs7padding(self, data):
        """明文使用PKCS7填充 """
        # if type(data) != str:
        #     print("序列")
        data = json.dumps(data)
        padding_size = 16 - len(data) % 16
        self.coding = chr(padding_size)
        padding_text = padding_size * self.coding
        data += padding_text
        return data.encode('utf-8')


    # 加密
    def encrypt(self,secret_key,secret_iv,data):
        try:
            secret_key = self.secret_key16(secret_key)
            secret_iv = self.secret_key16(secret_iv)
            data = self.pkcs7padding(data)
            # print("数据",data)

            # AES 加密
            aes = AES.new(secret_key,self.model,secret_iv)
            ciphertext = aes.encrypt(data)
            ciphertext = base64.b64encode(ciphertext)
            # ciphertext = binascii.b2a_hex(ciphertext)
            return ciphertext.decode("utf-8")
        except Exception as e:
            print("加密错误",e)
            return False

    # 解密
    def decrypt(self,secret_key,secret_iv,data):
        try:
            secret_key = self.secret_key16(secret_key)
            secret_iv = self.secret_key16(secret_iv)

            # data = binascii.a2b_hex(data)
            data = base64.b64decode(data)
            # AES 解密
            aes = AES.new(secret_key, self.model,secret_iv)
            decrypted = aes.decrypt(data)

            data = decrypted.decode("utf-8")
            # 去除解码后的非法字符
            try:
                data = re.compile('[\\x00-\\x08\\x0b-\\x0c\\x0e-\\x1f\n\r\t]').sub('', data)
            except Exception:
                data = '解码失败，请重试!'
            return json.loads(data)
        except Exception as e:
            print("解密错误",e)
            return False


# if __name__ == '__main__':
#     ase = Aes_Encrypt()
#     data = {'name':12,"age":123}
#
#     data = ase.encrypt("123",2,data)
#     print("加密",data)
#     data = ase.decrypt("123",2,data)
#     print("返回",data)
